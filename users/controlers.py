import base64

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.core.files.base import ContentFile
from django.utils.crypto import get_random_string

from .models import User


def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        new_user = User.objects.create_user(username, email, password)
        return JsonResponse({
            'success': True,
        })


def login_contr(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return JsonResponse({
                'success': True,
            })
        return JsonResponse({
            'success': False,
        })


@login_required(login_url='/users/login/')
def test(request):
    return JsonResponse({
        'success': True,
    })


@login_required(login_url='/users/login/')
def points(request):
    points = request.user.points
    return JsonResponse({
        'success': True,
        'points': points,
    })


@login_required(login_url='/users/login/')
def profile(request):
    return JsonResponse({
        'success': True,
        'username': request.user.username,
        'points': request.user.points,
    })


@login_required(login_url='/users/login/')
def avatar(request):
    if request.method == 'POST':
        data = request.POST['image']
        format, imgstr = data.split(';base64,')
        extension = format.split('/')[-1]
        image_file = ContentFile(base64.b64decode(
            imgstr), name=get_random_string(length=32) + '.' + extension)
        request.user.avatar = image_file
        request.user.save()
        return JsonResponse({
            'success': True,
        })
    elif request.method == 'GET':
        avatar = request.user.avatar
        fd = open(avatar.path, 'rb')
        return HttpResponse(fd.read())
