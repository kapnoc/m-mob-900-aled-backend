from django.urls import path

from . import controlers

urlpatterns = [
    # path('', views.index, name='index'),
    path('register/', controlers.register, name='register'),
    path('login/', controlers.login_contr, name='login'),
    path('avatar/', controlers.avatar, name='avatar'),
    path('test/', controlers.test, name='test'),
    path('points/', controlers.points, name='points'),
    path('profile/', controlers.profile, name='profile'),
]
