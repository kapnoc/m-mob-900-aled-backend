from django.urls import path

from . import controlers

urlpatterns = [
    path('', controlers.plants, name='plants'),
    path('upgrade/<int:id>', controlers.upgrade, name='upgrade'),
    path('harvest/<int:id>', controlers.harvest, name='harvest'),
]
