from datetime import datetime, timezone
from decimal import Decimal

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.forms import modelform_factory
from geopy import distance
from geographiclib.geodesic import Geodesic

from .models import Plant

geod = Geodesic.WGS84


@login_required(login_url='/users/login/')
def plants(request):
    if request.method == 'POST':
        PlantForm = modelform_factory(model=Plant, exclude=['created_at'])
        aled = {
            'latitude': request.POST['latitude'],
            'longitude': request.POST['longitude'],
        }
        aled['user'] = request.user.id
        aled['level'] = 1
        aled['last_harvested'] = datetime.now()
        new_plant = PlantForm(aled)
        if not new_plant.is_valid():
            print(new_plant.errors)
        new_plant.save()
        request.user.points -= 1000
        request.user.save()
        return JsonResponse({
            'success': True,
        })

    # GET
    user_lat = request.GET.get('lat', None)
    user_lon = request.GET.get('lon', None)
    if user_lat is None or user_lon is None:
        return JsonResponse({
            'success': False,
            'error': 'Latitude and longitude not supplied as GET parameters',
        })
    user_lat = Decimal(value=user_lat)
    user_lon = Decimal(value=user_lon)
    user_plants = Plant.objects.filter(
        user=request.user,
    )
    plants = []
    for plant in user_plants:
        dist = distance.distance((user_lat, user_lon),
                                 (plant.latitude, plant.longitude)).km * 1000
        if dist < 500:
            points = (datetime.now(timezone.utc) -
                      plant.last_harvested).total_seconds()
            points *= plant.level
            direction_angle = geod.Inverse(
                user_lat, user_lon, plant.latitude, plant.longitude)
            plants.append({
                'id': plant.id,
                'latitude': plant.latitude,
                'longitude': plant.longitude,
                'distance': dist,
                'level': plant.level,
                'last_harvested': plant.last_harvested.strftime("%m/%d/%Y, %H:%M:%S"),
                'points': points,
                'direction': direction_angle['azi1'],
            })
    return JsonResponse({
        'success': True,
        'plants': plants,
    })


@login_required(login_url='/users/login/')
def harvest(request, id: int):
    plant = Plant.objects.get(id=id)
    points = (datetime.now(timezone.utc) -
              plant.last_harvested).total_seconds()
    points *= plant.level
    plant.last_harvested = datetime.now()
    plant.save()
    request.user.points += points
    request.user.save()
    return JsonResponse({
        'success': True,
        'user_points': request.user.points,
    })


@login_required(login_url='/users/login/')
def upgrade(request, id: int):
    plant = Plant.objects.get(id=id)
    request.user.points -= 1000 * plant.level
    points = (datetime.now(timezone.utc) -
              plant.last_harvested).total_seconds()
    points *= plant.level
    plant.last_harvested = datetime.now()
    plant.level += 1
    plant.save()
    request.user.points += points
    request.user.save()
    return JsonResponse({
        'success': True,
    })
