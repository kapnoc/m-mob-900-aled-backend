from django.db import models

from users.models import User


class Plant(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    latitude = models.FloatField()
    longitude = models.FloatField()
    level = models.PositiveIntegerField(blank=True, default=1)
    last_harvested = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
